#include <iostream>
#include <vector>
#include <fstream>

using namespace std;

class PunctCounter{
    public:
        vector<char> symbols {'.', ',', '!', '?', ';', ':', '-'};

        void start(const string& ifName, int bufSize){
            ifstream in(ifName, ifstream::ate | ifstream::binary);
            if (!in){
                throw "Ошибка чтения файла!";
            };

            vector<vector<char> > fileInfo;
            int fileSize = in.tellg();
            in.seekg(0);
            while (fileSize >= bufSize){
                fileInfo.push_back(readBlock(in, bufSize));
                fileSize -= bufSize;
            };

            if (fileSize > 0) {
                fileInfo.push_back(readBlock(in, fileSize));
            };
            in.close();

            int count = 0;
            for (vector<char> i : fileInfo){
                count += countPrepSymbols(i);
            }

            cout << "Количество символов пунктуации в файле = " << count << endl;
        };

        // чтение блока заданной длины
        static vector<char> readBlock(ifstream& stream, int size){
            vector<char> block (size, 0);
            stream.read(&block[0], size);
            return block;
        };

        //подсчёт знаков в блоке
        int countPrepSymbols(vector<char> block){
            int counter = 0;

            for (char i : block){
                if (find(symbols.begin(), symbols.end(), i) != symbols.end()){
                    counter++;
                }
            }

            return counter;
        }
};

int main(int argc, char **argv) {
    try {
        string help = "Использование: имя_программы (-h = вывести эту подсказку) путь/до/файла.txt";

        if (argc == 1){
            cout << "Не заданы аргументы!" << endl << help << endl;
            return 1;
        } else if (strcmp(argv[1], "-h") == 0){
            cout << help << endl;
            return 1;
        } else {
            PunctCounter count;
            count.start(argv[1], (argc == 3) ? stoi(argv[2]) : 4);
            return 0;
        };
    } catch (const char* exception) {
        cerr << exception << endl;
        return 1;
    };
};
