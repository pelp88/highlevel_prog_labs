#include <iostream>
#include <vector>
#include <fstream>

using namespace std;

class FileCopy{
    public:
        void copy(const string& ifName, const string& ofName1, const string& ofName2, int bufSize){
            ifstream in(ifName, ifstream::ate | ifstream::binary);
            if (!in){
                throw "Ошибка чтения файла!";
            };

            vector<vector<char> > fileInfo;
            int fileSize = in.tellg();
            in.seekg(0);
            while (fileSize >= bufSize){
                fileInfo.push_back(readBlock(in, bufSize));
                fileSize -= bufSize;
            };

            if (fileSize > 0) {
                fileInfo.push_back(readBlock(in, fileSize));
            };
            in.close();

            ofstream out1(ofName1, ifstream::out | ifstream::binary);
            if (!out1){
                throw "Ошибка записи файла #1!";
            };
            ofstream out2(ofName2, ifstream::out | ifstream::binary);
            if (!out2){
                throw "Ошибка записи файла #2!";
            };

            for (vector<char> i : fileInfo) {
                writeBlock(out1, i);
                writeBlock(out2, i);
            };
            out1.close();
            out2.close();
        };

    private:
        // чтение блока заданной длины
        static vector<char> readBlock(ifstream& stream, int size){
            vector<char> block (size, 0);
            stream.read(&block[0], size);
            return block;
        };


        // записать блок в файл
        static void writeBlock(ofstream& stream, vector<char> block) {
            for (char i: block) {
                stream << i;
            };
        };
};

int main(int argc, char **argv) {
    try {
        string help = "Использование: имя_программы (-h = вывести эту подсказку) путь/до/исходного_файла.txt путь/до/копии1.txt путь/до/копии2.txt";

        if (argc == 1){
            cout << "Не заданы аргументы!" << endl << help << endl;
            return 1;
        } else if (strcmp(argv[1], "-h") == 0){
            cout << help << endl;
            return 1;
        } else {
            FileCopy copy;
            copy.copy(argv[1], argv[2], argv[3], (argc == 5) ? stoi(argv[4]) : 4);
            return 0;
        };
    } catch (const char* exception) {
        cerr << exception << endl;
        return 1;
    };
};
