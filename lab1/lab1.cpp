#include <iostream>
#include <iomanip>
using namespace std;

class Calculator{
    public:
        void convert(){
            string input;
            cout << "Enter the value: ";
            cin >> input;
            input.erase(input.length() - 1);
            int res = convertToFarenheit(stoi(input));
            printAnswer(res);
        };

    private:
        // конвертер из градусов Цельсия в градусы Фаренгейта
        static int convertToFarenheit(int celsius) {
            return (int) (celsius * 1.8);
        };

        // манипулятор - десятичный вывод и точность 6 знаков
        static ostream& decPrecision(ostream& stream) {
            return stream << fixed << dec << setprecision(6);
        };

        // манипулятор - вывод сообщения
        static ostream& printTest(ostream& stream) {
            return stream << "Тест";
        };

        // вывести ответ в формате по заданию
        static void printAnswer(int temp){
            double example = 123.4567890;
            cout << setfill('$') << setw(10) << "Dec:" << dec << temp << 'F' << endl;
            cout << setfill('$') << setw(10) << "Hex:" << hex << temp << 'F' << endl;
            cout << endl;
            cout << "Flags:" << cout.flags() << endl;
            cout << "Обьчный вывод:" << example << endl;
            cout << "Научный вывод:" << scientific << example << endl;
            cout << "Без вывода основания:" << scientific << noshowbase << example << endl;
            cout << "Свой манипулятор - свой текст:" << printTest << endl;
            cout << "Свой манипулятор - десятичный вывод и точность 6 знаков:" << decPrecision << example << endl;
        };
};

int main() {
    Calculator calc;
    calc.convert();
    return 0;
}
